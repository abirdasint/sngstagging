<?php

/**
 * @file
 * Password policy constraint callbacks.
 */

/****************************************************************************/
/* Constraint API                                                           */
/****************************************************************************/

/**
 * Description of the constraint.
 */
function password_policy_constraint_dchar_description() {
  return array('name' => t('Dchar'), 'description' => t('Password must contain the specified minimum number of dchar letters.'));
}

/**
 * Error message of the constraint.
 */
function password_policy_constraint_dchar_error($constraint) {
  return format_plural($constraint, 'No character repeatation is allowed.', 'No character repeatation is allowed.');
}

/**
 * Password validation.
 */
function password_policy_constraint_dchar_validate($password, $constraint, $account) {
  $chars = drupal_strlen($password);
  $parray = str_split(strtolower($password), 1);
  $num = 0;
  return count($parray) == count(array_unique($parray));  
}

/**
 * Javascript portion.
 */
function password_policy_constraint_dchar_js($constraint, $account) {
  return <<<JS
var arr = [];
var ex = 0;
    var i = 0;
    var num = 0;
    var chr = '';   
    while (i < value.length) {
      chr = value.charAt(i);
     
    if(jQuery.inArray(chr.toLowerCase(), arr) >= 0){        
        ex = 1;
    }    
      arr.push(chr.toLowerCase());
     
      i++;
    }
    if (ex==1) {
      strength = 'low';
      msg.push(translate.constraint_dchar);
    }
JS;
}
