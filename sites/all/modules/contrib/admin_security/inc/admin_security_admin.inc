<?php
/**
 * Menu callback. Display Allowed Admin IP addresses.
 *
 * @param $default_ip
 *   Optional IP address to be passed on to drupal_get_form() for
 *   use as the default value of the IP address form field.
 */

function admin_security($default_ip = '')
{
  drupal_add_js(drupal_get_path('module', 'admin_security') . '/inc/admin_security.js', array('type' => 'file', 'preprocess' => FALSE));
  $rows = array();
  $header = array(t('Allowed IP addresses'), t('Operations'));
  $result = db_query('SELECT * FROM {admin_security}');
  foreach ($result as $ip) {
    $rows[] = array(
        $ip->ip,
        _admin_security_op($ip),
    );
  }

  $build['admin_security_form'] = drupal_get_form('admin_security_form', $default_ip);

  $build['admin_security_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No allowed IP addresses available.'),
  );

  return $build;
}

function _admin_security_op($ip)
{
  $enabled = variable_get('admin_security_enabled', 0);
  if ($enabled && $ip->ip == ip_address()) {
    return "current IP, can't delete";
  } else {
    return l(t('delete'), "admin/config/people/admin_security/delete/$ip->iid");
  }
}

/**
 * Define the form for Allowed Admin IP addresses.
 *
 * @ingroup forms
 * @see admin_security_form_validate()
 * @see admin_security_form_submit()
 */
function admin_security_form($form, $form_state, $default_ip)
{
  $enabled = variable_get('admin_security_enabled', 0);
  $user_enabled = variable_get('admin_security_login_blocking_enabled', 0);
  $form['help'] = array(
      '#type' => 'markup',
      '#markup' => t('
<p>When this module is activated below, it blocks all access to the Admin pages except for requests from the IP numbers specified below.</p>
<p>!ip_msg</p>
<p>You can override these settings in "settings php".<br />
"$conf[\'admin_security_enabled\'] = 0;" deactivates the module.<br />
"$conf[\'admin_security_login_blocking_enabled\'] = 0;" deactivates the optional login blocking only.<br />
"$conf[\'admin_security_ips\'] = array(\'xxx.xxx.xxx.xxx\'); IPs in this array will override any entered below."</p>',
          array('!ip_msg' => _admin_security_user_current_ip_msg())),
  );
  $form['enabled'] = array(
      '#title' => t('ACTIVATE admin page blocking'),
      '#type' => 'checkbox',
      '#default_value' => $enabled,
      '#description' => t('When enabled for the first time, your current IP number will be saved.'),
  );
  $form['login_blocking_enabled'] = array(
      '#title' => t('ACTIVATE login blocking'),
      '#type' => 'checkbox',
      '#default_value' => $user_enabled,
      '#disabled' => _admin_security_user_checkbox_state($enabled),
      '#description' => t('Optionally block the login page and ALL user pages from unapproved IPs.'),
  );
  $form['ip'] = array(
      '#title' => t('IP address'),
      '#type' => 'textfield',
      '#size' => 48,
      '#maxlength' => 40,
      '#description' => t('Enter a valid IP address or address range. Valid formats are...<br />1. Single IP number: 1.2.3.4<br />2. Wildcard format: 1.2.3.*<br />3. CIDR format: 1.2.3/24  OR  1.2.3.4/255.255.255.0<br />4. Start-End IP format: 1.2.3.0-1.2.3.255'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
  );
  $form['#submit'][] = 'admin_security_form_submit';
  $form['#validate'][] = 'admin_security_form_validate';
  return $form;
}

function _admin_security_user_checkbox_state($enabled) {
  if ($enabled) {
    return FALSE;
  } else {
    return TRUE;
  }
}

function _admin_security_user_current_ip_msg() {
  $curr_ip = ip_address();
  $output = t('Your current IP is @ip.', array('@ip' => $curr_ip)) . '<br />';
  if ($curr_ip == '127.0.0.1') {
    $output .= t('The localhost IP is automatically allowed. There is no need to add it.');
  } else {
    $output .= t('This will be added automatically when you activate the module. You do not need to specify it.');
  }
  return $output;
}

function admin_security_form_validate($form, &$form_state) {
  $ip = trim($form_state['values']['ip']);

  if ($ip == '127.0.0.1') {
    form_set_error('ip', t('You do not need to add the localhost IP, it is always allowed access.'));
  } else if (db_query("SELECT * FROM {admin_security} WHERE ip = :ip", array(':ip' => $ip))->fetchField()) {
    form_set_error('ip', t('This IP address is already saved.'));
  }
}

function admin_security_form_submit($form, &$form_state) {
  variable_set('admin_security_enabled', $form_state['values']['enabled']);
  variable_set('admin_security_login_blocking_enabled', $form_state['values']['login_blocking_enabled']);

  $stored_ips = (bool)db_query("SELECT 1 FROM {admin_security}")->fetchField();
  $ip = trim($form_state['values']['ip']);
  $current_ip = ip_address();

  // If there are no stored IPs store the current IP so we don't lock out the user.
  // Localhost is always allowed, so it's ignored
  if (!$stored_ips && $current_ip != '127.0.0.1' && $ip != $current_ip) {
    db_insert('admin_security')
        ->fields(array('ip' => $current_ip))
        ->execute();
    drupal_set_message(t('The default IP address @ip has been added.', array('@ip' => $current_ip)));
  }

  // Store the submitted IP.
  if ($ip != '') {
    $ip = str_replace(' ', '', $ip);
    db_insert('admin_security')
        ->fields(array('ip' => $ip))
        ->execute();
    drupal_set_message(t('The IP address @ip has been added.', array('@ip' => $ip)));
  }

  $form_state['redirect'] = 'admin/config/people/admin_security';
  return;
}

/**
 * IP deletion confirm page.
 *
 * @see admin_security_delete_submit()
 */
function admin_security_delete($form, &$form_state, $iid) {
  $ip = db_query("SELECT * FROM {admin_security} WHERE iid = :iid", array(':iid' => $iid))->fetchAssoc();
  $form['iid'] = array(
      '#type' => 'value',
      '#value' => $iid,
  );
  $form['ip'] = array(
      '#type' => 'hidden',
      '#value' => $ip['ip'],
  );
  return confirm_form($form, t('Are you sure you want to delete @ip?', array('@ip' => $ip['ip'])), 'admin/config/people/admin_security', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Process admin_security_delete form submissions.
 */
function admin_security_delete_submit($form, &$form_state) {
  db_delete('admin_security')
      ->condition('iid', $form_state['values']['iid'])
      ->execute();
  watchdog('admin_security', 'Deleted allowed admin IP @ip', array('@ip' => $form_state['values']['ip']));
  drupal_set_message(t('The IP address @ip was deleted.', array('@ip' => $form_state['values']['ip'])));
  $form_state['redirect'] = 'admin/config/people/admin_security';
}

/**
 * @param string $default_ip
 * @return mixed
 */
function admin_security_optional_paths($default_path = '')
{
  drupal_add_js(drupal_get_path('module', 'admin_security') . '/inc/admin_security.js', array('type' => 'file', 'preprocess' => FALSE));
  $rows = array();
  $header = array(t('Optional Paths'), t('Operations'));
  $result = variable_get('admin_security_optional_paths', array());
  foreach ($result as $path) {
    $rows[] = array(
        $path,
        _admin_security_optional_paths_op($path),
    );
  }

  $build['admin_security_optional_paths_form'] = drupal_get_form('admin_security_optional_paths_form', $default_path);

  $build['admin_security_optional_paths_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No optional paths.'),
  );

  return $build;
}

function _admin_security_optional_paths_op($path)
{
  $path = urlencode($path);
  return l(t('delete'), "admin/config/people/admin_security/optional_paths/delete/$path");
}

function admin_security_optional_paths_form($form, $form_state, $default_ip)
{
  $form['help'] = array(
      '#type' => 'markup',
      '#markup' => t('<p>This module automatically blocks the core admin urls, "user/*", "users/*", "node/*/xxx", "taxonomy/*/xxx", "admin/*".</p><p>Contrib modules should build their admin interfaces off of one of these paths but that\'s by no means guaranteed. So you can add custom admin urls here.</p>'),
  );
  $form['optional_path'] = array(
      '#title' => t('Path'),
      '#type' => 'textfield',
      '#size' => 48,
      '#maxlength' => 40,
      '#description' => t('Enter a valid Drupal path. No leading or trailing slash. No wildcards'),
      '#required' => TRUE,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
  );
  $form['#submit'][] = 'admin_security_optional_paths_form_submit';
  $form['#validate'][] = 'admin_security_optional_paths_form_validate';
  return $form;
}

function admin_security_optional_paths_form_validate($form, &$form_state) {
  $optional_paths = variable_get('admin_security_optional_paths', array());

  if (in_array($form_state['values']['optional_path'], $optional_paths)) {
    form_set_error('optional_path', t('This path has already been added to the Admim Security configuration.'));
  }
  else if (substr($form_state['values']['optional_path'], 0, 1) == '/') {
    form_set_error('optional_path', t('Do not include a leading slash on the path.'));
  } else if ((substr($form_state['values']['optional_path'], -1, 1) == '/')) {
    form_set_error('optional_path', t('Do not include a trailing slash on the path.'));
  }
}

function admin_security_optional_paths_form_submit($form, &$form_state) {
  $optional_paths = variable_get('admin_security_optional_paths', 0);
  if ($optional_paths) {
    $optional_paths[] = $form_state['values']['optional_path'];
  }
  else {
    $optional_paths = array($form_state['values']['optional_path']);
  }
  variable_set('admin_security_optional_paths', $optional_paths);
  drupal_set_message(t('The path @path has been added.', array('@ip' => $form_state['values']['optional_path'])));
  $form_state['redirect'] = 'admin/config/people/admin_security/optional_paths';
  return;
}

/**
 * Path deletion confirm page.
 *
 * @see admin_security_optional_paths_delete_submit()
 */
function admin_security_optional_paths_delete($form, &$form_state, $path) {
  $path = urldecode($path);
  $form['path'] = array(
      '#type' => 'hidden',
      '#value' => $path
  );
  return confirm_form($form, t('Are you sure you want to delete the path "@path"?', array('@path' => $path)), 'admin/config/people/admin_security/optional_paths', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Process admin_security_optional_paths_delete form submissions.
 */
function admin_security_optional_paths_delete_submit($form, &$form_state) {
  $optional_paths = variable_get('admin_security_optional_paths', 0);
  $index = array_search($form_state['values']['path'], $optional_paths);
  if ($index !== FALSE) {
    array_splice($optional_paths, $index, 1);
  }
  variable_set('admin_security_optional_paths', $optional_paths);
  watchdog('admin_security', 'Deleted optional path "@path" from Admin Security', array('@path' => $form_state['values']['path']));
  drupal_set_message(t('The path "@path" was deleted.', array('@path' => $form_state['values']['path'])));
  $form_state['redirect'] = 'admin/config/people/admin_security/optional_paths';
}
