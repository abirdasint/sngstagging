<?php
function admin_security_menu() {
  $items['admin/config/people/admin_security'] = array(
      'title' => 'Admin Security',
      'description' => 'Manage allowed IPs for Admin.',
      'page callback' => 'admin_security',
      'access arguments' => array('administer admin security IP addresses'),
      'file' => 'inc/admin_security_admin.inc',
  );
  $items['admin/config/people/admin_security/settings'] = array(
      'title' => 'Admin Security Settings',
      'description' => 'Manage allowed IPs for Admin.',
      'page callback' => 'admin_security',
      'access arguments' => array('administer admin security IP addresses'),
      'file' => 'inc/admin_security_admin.inc',
      'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/config/people/admin_security/optional_paths'] = array(
      'title' => 'Optional Paths',
      'description' => 'Manage optional paths for Admin Security.',
      'page callback' => 'admin_security_optional_paths',
      'access arguments' => array('administer admin security IP addresses'),
      'file' => 'inc/admin_security_admin.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => 2,
  );
  $items['admin/config/people/admin_security/delete/%'] = array(
      'title' => 'Delete IP address',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('admin_security_delete', 5),
      'access arguments' => array('administer admin security IP addresses'),
      'file' => 'inc/admin_security_admin.inc',
  );
  $items['admin/config/people/admin_security/optional_paths/delete/%'] = array(
      'title' => 'Delete Path',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('admin_security_optional_paths_delete', 6),
      'access arguments' => array('administer admin security IP addresses'),
      'file' => 'inc/admin_security_admin.inc',
  );
  return $items;
}

function admin_security_permission() {
  return array(
      'administer admin security IP addresses' => array(
          'title' => t('administer admin security IP addresses'),
          'description' => t('Administer the IPs that allow access to the admin pages.'),
      ),
  );
}

function admin_security_init() {
  if (variable_get('admin_security_enabled', 0)) {
    if (_admin_security_parseurl()) {
      module_load_include('inc', 'admin_security', 'inc/admin_security_ip_check');
      $current_ip = ip_address();
      if ($current_ip != '127.0.0.1') {
        $allowed = FALSE;
        // Because this function is called on every page request, we first check
        // for an array of IP addresses in settings.php before querying the
        // database.
        $allowed_ips = variable_get('admin_security_ips');
        if (isset($allowed_ips) && is_array($allowed_ips)) {
          $allowed = _admin_security_ip_is_allowed($current_ip, $allowed_ips);
        }
        // Only check if database.inc is loaded already. If
        // $conf['page_cache_without_database'] = TRUE; is set in settings.php,
        // then the database won't be loaded here so the IPs in the database
        // won't be allowed. However the user asked explicitly not to use the
        // database and also in this case it's quite likely that the user relies
        // on higher performance solutions like a firewall.
        elseif (class_exists('Database', FALSE)) {
          $allowed = _admin_security_ip_is_allowed($current_ip);
        }
        // Deny access if an allowed IP address is not available.
        if (!$allowed) {
          watchdog('admin_security', 'An admin page request from IP @ip was blocked.', array('@ip' => $current_ip));
          header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
          print 'Sorry, ' . check_plain(ip_address()) . ' has been banned from site administration.';
          exit();
        }
      }
    }
  }
}

/**
 * Performs a check of the current IP against the allowed IP(s)
 * @arg $current_ip: The current ip of the http request
 * @arg $allowed_from_settings: (optional) This is the array provided from settings.php with
 * variable_get('admin_security_ips'). If passed, these hardcoded values override the IPs saved
 * in the modules UI.
 */
function _admin_security_ip_is_allowed($current_ip, $allowed_from_settings = array()) {
  $output = FALSE;
  $allowed_ips = array();
  if ($allowed_from_settings) {
    // Derive the allowed IPs form the line in settings.php
    $allowed_ips = $allowed_from_settings;
  } else {
    // Derive the allowed IPs from the DB
    $allowed_ips_result = db_query("SELECT ip FROM {admin_security}");
    foreach ($allowed_ips_result as $ip) {
      $allowed_ips[] = $ip->ip;
    }
  }
  foreach ($allowed_ips as $allowed) {
    if (strpos($allowed, '-') === FALSE &&
        strpos($allowed, '*') === FALSE &&
        strpos($allowed, '/') === FALSE
    ) {
      // This is an explicit IP number
      // Allow access if there is an exact match.
      if ($current_ip == $allowed) {
        $output = TRUE;
      }
    }
    else {
      // This is a range of some sort
      // Run the IP through the function in admin_security_ip_check.inc to test for authorization.
      if (admin_security_ip_in_range($current_ip, $allowed)) {
        $output = TRUE;
      }

    }
  }
  return $output;
}

/**
 * Parses the request path. Returns true if it's an admin url.
 */
function _admin_security_parseurl()
{
  $user_blocking_enabled = variable_get('admin_security_login_blocking_enabled', 0);
  $optional_paths = variable_get('admin_security_optional_paths', array());
  $path = explode('/', current_path());
  // React to all admin pages
  if (isset($path[0])) {
    if ($path[0] == 'admin' ||
        ($path[0] == 'node' && (isset($path[1]) && $path[1] == 'add')) ||
        ($path[0] == 'node' && (isset($path[2]) && $path[2] == 'edit')) ||
        ($path[0] == 'node' && (isset($path[2]) && $path[2] == 'delete')) ||
        ($path[0] == 'node' && (isset($path[2]) && $path[2] == 'revisions')) ||
        ($path[0] == 'taxonomy' && (isset($path[3]) && $path[3] == 'edit'))
    ) {
      return TRUE;
    } // React to user paths
    elseif ($path[0] == 'user') {
      global $user;
      // React to user page that does not match the current user
      if (isset($path[1]) && $user->uid != $path[1]) {
        if (($path[0] == 'user' && $path[2] == 'edit') ||
            ($path[0] == 'user' && $path[2] == 'cancel')
        ) {
          return TRUE;
        }
      }
    } // Block user listing page
    elseif ($path[0] == 'users') {
      return TRUE;
    }
    // Block user page if the optional user login blocking is enabled.
    if ($user_blocking_enabled && $path[0] == 'user') {
      return TRUE;
    }
    // Block any custom admin paths.
    if ($optional_paths && in_array(current_path(), $optional_paths)) {
      return TRUE;
    }
  }
}

/**
 * This is to prevent logins from the login block and all other sorts of login attempts.
 * It doesn't prevent the login. It just immediately logs the user back out and redirects to the blocking announcement
 */
function admin_security_user_login(&$edit, $account) {
  $user_blocking_enabled = variable_get('admin_security_login_blocking_enabled', 0);
  // Deny access if an user logins are not allowed.
  if ($user_blocking_enabled) {
    // Log the user out
    global $user;
    $username = $user->name;
    module_invoke_all('user_logout', $user);
    // Destroy the current session, and reset $user to the anonymous user.
    session_destroy();

    watchdog('admin_security', 'User "@username" attempted to login from IP @ip and was blocked.', array('@ip' => ip_address(), '@username' => $username));
    header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
    print 'Sorry, the IP ' . check_plain(ip_address()) . ' has been blocked from site login.';
    exit();
  }
}
