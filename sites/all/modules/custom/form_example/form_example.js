(function($, Drupal)
{
  function showPopup()
  {
    $('.after-contactus-submit').show();
    $('.mobile_menu_backdrop').show();
  }
  
  //Added as on: 16-11-2017, to check whether the given string is html
	
	function isHTML(str) {
		var a = document.createElement('div');
		a.innerHTML = str;
		for (var c = a.childNodes, i = c.length; i--; ) {
			if (c[i].nodeType == 1) return true; 
		}
		return false;
	}	

   function validation(name)
  {
    //if($('#edit-description').val()==''){
    //  $('#edit-description').addClass('error');
    //   $('.description-val').show();
    //}else{
    //  $('.description-val').hide();
    //}
	
	var description =   $('#edit-description').val();
	  
	if(isHTML(description)){
		$('#edit-description').parent().addClass('error');
		$('.description-val').show();
        $('.description-val').html('* Html characters are not allowed.');
		
		
	}else{
		$('.description-val').html('');
		$('.description-val').hide();
		$('#edit-description').parent().removeClass('error');
	}  
	
    if($('#edit-name').val()==''){
      $('#edit-name').parent().addClass('error');
      $('.name-val').show();
    }else{
      $('.name-val').hide();
      $('#edit-name').parent().removeClass('error');
    }
    if($('#edit-requester').val()==''){
      $('#edit-requester').parent().addClass('error');
      $('.requester-val').html('*Email is required');
       $('.requester-val').show();
    }else{
       var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (!expr.test($('#edit-requester').val())) {
         $('#edit-requester').parent().addClass('error');
        $('.requester-val').html('*Email is not valid');
        $('.requester-val').show();
      }else{
        $('#edit-name').parent().removeClass('error');
        $('.requester-val').hide();
      }
      //$('#edit-name').val();
      //$('#edit-requester').val();
    }
  }

  // Create the callback for the command we used in our ajax response
  Drupal.ajax.prototype.commands.formPopupTriggerPopup = function(hello,type)
  {
    // response.name contains the value the user submitted in the form.
    // We will pass this to our function showPopup, so it can be shown in the popup.
   //console.log(type.name);
    if (type.name==1 || type.captcha==1) {
        //code
        
        if (type.captcha==1) {
          console.log(type.captcha);
           $('.captcha-error').html('Please enter correct captcha.');
        }else{
           $('.captcha-error').html('')
        }
        validation(type.captcha);
        
    }else{
      validation();
       showPopup();
    }
   
  };
}(jQuery, Drupal));
