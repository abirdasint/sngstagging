<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */ 
 $pagetitle = drupal_get_title();

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
<meta name="description" content="Singapore Life is a 100% digital life insurance provider that offers you value through technology. Born in Singapore, we’re your next generation life insurance."/>
<head profile="<?php print $grddl_profile; ?>">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0"/>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet"/> 
  
  <?php print $styles; ?>
  <?php
	global $base_url;
	global $theme;
	global $base_path;
	$theme_path = drupal_get_path('theme', $theme); 
  ?>
  <?php if ($is_front): ?>
 <link type="text/css" rel="stylesheet" media="all" href="<?php echo $theme_path; ?>/css/animate.css"/>
<?php endif; ?>
<script type="text/javascript">
	var themeurl = '<?php echo $theme_path; ?>';
</script>


  <?php print $scripts; ?>


<script type="text/javascript">
window.zESettings = {
  webWidget: {
    color: {
      theme: '#f41331'    
    },
   helpCenter : {
     title: {
        'en-US': 'Hello'
      }
    },    
    contactForm: {
      title: {
        'en-US': 'Contact Us'
      },
      attachments: false
    },
  }
};

</script>

<script type="text/javascript">

  jQuery( document ).ready(function() {
	//<![CDATA[
	
    var helpCenterExists = setInterval(function() {  
 
var check = 1;
var textall = 1;
jQuery("body").find("iframe").each(function(){
  
  if(jQuery(this).attr('id')!='homevideoyoutube' && jQuery(this).attr('class')!='cboxIframe'){	
  if (jQuery(this).contents().find('head').html()=='') {
	check = 2;
     jQuery(this).contents().find('head').html('<link type="text/css" rel="stylesheet" href="'+themeurl+'/css/chat.css" media="all"/>');
  }
  //console.log(jQuery(this).contents().find('.offline_msg'));
  //clearInterval(helpCenterExists);
  if(jQuery(this).contents().find('header').text().trim() != ''){
	//console.log(jQuery(this).contents().find('header').text());
	//console.log(jQuery(this).contents().find('.offline_msg').text());
	if(jQuery(this).contents().find('.offline_msg').text()==''){
	  textall = 2;
	  jQuery(this).contents().find('header').after('<div class="offline_msg"><p>We are currently offline.Leave a message and we will contact you as soon as we can.</p><em>Live chats are available 9am-6pm, Monday - Friday</em></div>');
	}
	
  }
 // if (jQuery(this).contents().find('.title_text').text()=='') {
 //console.log(jQuery(this).find('.title_text').html());
     //jQuery(this).contents().find('.title_text').css('border' , '#000 1px solid');
	 if (jQuery(this).contents().find('.title_text').html()!='Customer Service') {
         jQuery(this).contents().find('.title_text').html('Customer Service');
     }
		
 // }
}
if (textall == 2 && check==1) {
  //console.log('hello');
	//clearInterval(helpCenterExists);  
  }

});

 // console.log(check+'hiii');

}, 1000);
 //]]>	

  // var helpCenterExists1 = setInterval(function() {
  //   //console.log('hiiii');  
  //   jQuery(".zopim").find("iframe").each(function(){
  //        if(jQuery(this).find('form')){
          
  //         jQuery(this).find('form').find('input').each(function(){
  //           console.log(jQuery(this).find('form'));
  //          });
  //        }
  //         //console.log('hiiii'); 
  //          });
  //   }, 1000);

});

</script>

<!-- Start of singaporelife Zendesk Widget script -->
<!-- Start of kinsuk Zendesk Widget script -->

<!-- End of kinsuk Zendesk Widget script -->
<!-- End of singaporelife Zendesk Widget script -->
<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-WC5JVBX');</script>

<!-- End Google Tag Manager -->
<script type="text/javascript">
  //              DO NOT IMPLEMENT                //
  //       this code through the following        //
  //                                              //
  //   Floodlight Pixel Manager                   //
  //   DCM Pixel Manager                          //
  //   Any system that places code in an iframe   //
    /*(function () {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
          + '.po.st/static/v4/post-widget.js#publisherKey=q53bh039ae3qv8hc2dqj';
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
     })();*/
</script>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WC5JVBX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="zendeskchat">
<!-- Start of kinsuk Zendesk Widget script -->
<!-- Start of singaporelife Zendesk Widget script -->
<!-- Start of kinsuk Zendesk Widget script -->
<!--<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="kinsuk.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/</script>-->
<!-- End of kinsuk Zendesk Widget script -->
<!-- End of singaporelife Zendesk Widget script -->
<!-- End of kinsuk Zendesk Widget script -->
  <!-- Start of singaporelife Zendesk Widget script -->
<script type="text/javascript">/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="singaporelife.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/</script>
<!-- End of singaporelife Zendesk Widget script -->
</div>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  <input type="hidden" id="base_url" value="<?php echo $base_url; ?>">
   <input type="hidden" id="titledesc" value="<?php echo $pagetitle; ?>">

  <script>
  jQuery(window).load(function() {
 
	var currentLocation = window.location;

 var titledsc = jQuery('#titledesc').val();
  //alert(titledsc);
	console.log(window.location.href);
	jQuery.ajax({
			type: "POST",	
			url:jQuery('#base_url').val()+"/api_curl/sugar.php",		       
			data:'url='+window.location.href+'&desc='+titledsc,					  
			success: function(data){
				
		  }
		});
    jQuery('#Embed').click(function(){
      console.log('hii');
    })
  });
		jQuery( window ).load(function() {
        console.log('test');
        jQuery(".expanded").each(function() {
                console.log(jQuery(this).find('a:first'));
                jQuery(this).find('a:first').attr("href", "#");
    
        });
    });
  </script>
</body>
<script type="text/javascript">
  //              DO NOT IMPLEMENT                //
  //       this code through the following        //
  //                                              //
  //   Floodlight Pixel Manager                   //
  //   DCM Pixel Manager                          //
  //   Any system that places code in an iframe   //
    //(function () {
    //    var s = document.createElement('script');
    //    s.type = 'text/javascript';
    //    s.async = true;
    //    s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
    //      + '.po.st/static/v4/post-widget.js#publisherKey=q53bh039ae3qv8hc2dqj';
    //    var x = document.getElementsByTagName('script')[0];
    //    x.parentNode.insertBefore(s, x);
    // })();
</script>
</html>
