<?php
$code = variable_get('getaquote');
global $base_url;
?>


 <div class="coverage_section">
      <div class="container">
      	<h2>Understand your coverage below:</h2>
      <h4 class="text-center">Learn what coverage others who are planning a family have.<span>But first, tell us a little about yourself...</span></h4>
      	<form method="post" action="">
      	<div class="coverage_form">      		
      			<div class="field_block">
      				<label>I am</label>
      				<div class="field">
      					<select style="width:120px">
      						<option>Gender</option>
      						<option value="male">Male</option>
      						<option value="female">Female</option>
      					</select>
      				</div>
      			</div>
      			<div class="field_block">
      				<label>I am</label>
      				<div class="field">
					  <input type="number" id="age" placeholder="Age" onkeypress="return AllowOnlyNumbers(event);">
					  
      				</div>
					  <span class="field-suffix age"><span class="requester-val error">*Age is required and <br/>should be between 18 to 55</span></span>
					  </div>
      			<div class="field_block">
      				<label>My annual income is SGD</label>
      				<div class="field">
      				<input type="number" id="income" placeholder="00,000" onkeypress="return AllowOnlyNumbers(event);">
      				</div>
					  <span class="field-suffix income"><span class="requester-val error">*Annual income is required and <br/>must be greater than 5000</span></span>
					  </div>
      			      		
      	</div>
      	<div class="btn_block">
      					<button type="button" class="btn primary-btn" onclick="calccov();">Check coverage

               

          </button>
      			</div>
				<input type="hidden" id="base_url" value="<?php echo $base_url; ?>">
      	</form>
		<style>
		  .tab_content {
			   display: none;
		  }
		  #life-cover {
			   display: block;
		  }
		  .field-suffix{
			   display: none;
		  }
		  .coverage_result_container{
			   display: none;
		  }
		</style>
		<script>		 
		  jQuery( document ).ready(function() {
			   jQuery('.tabs').click(function(){
					jQuery('.tabs').removeClass('active');
					jQuery('.tab_content').hide();
					jQuery('#'+jQuery(this).attr('rel')).show();
					jQuery(this).addClass('active');
			   });
		  });
		  
		  function calccov() {
			   if (jQuery('#age').val()==''  || jQuery('#age').val() < 18 || jQuery('#age').val() > 55) {
					jQuery('.age').css('display','block');
					return false;
               }else{
					jQuery('.age').hide();
			   }
			   if (jQuery('#income').val()==''  || jQuery('#income').val() < 5000) {
					jQuery('.income').css('display','block');
					return false;
               }else{
					jQuery('.income').hide();
			   }
			   
			   jQuery('#age').val();
			   var base;
			   var ci;
			   
			   if (jQuery('#age').val() < 30) {
					base = 0;
					ci = 0;
					wp = 4.09;
					cl = 6.37;
               }
			   if (jQuery('#age').val() < 40 && jQuery('#age').val() >= 30) {
					base = -1;
					ci = -0.5;
					wp = 5.35;
					cl = 3.72;
               }
			   if (jQuery('#age').val() < 50 && jQuery('#age').val() >= 40) {
					base = -2;
					ci = -1;
					wp = 3.02;
					cl = 1.86;
               }
			   if (jQuery('#age').val() >= 50) {
					base = -3;
					ci = -1.5;
					wp = 4.44;
					cl = 11.94;
               }
			   var mindc = 10;
			   var maxdc = 16;
			   
			   var minci = 5;
			   var maxci = 8;
			   
			   console.log(base);
			   var mindeathcover = (mindc + base) *  jQuery('#income').val();
			   var maxdeathcover = (maxdc + base) *  jQuery('#income').val();
			   var l1 = wp *  jQuery('#income').val();

			   jQuery('.mindeathcover').text('SGD'+addCommas(Math.round(l1)));
			    jQuery('.maxdeathcover').text('SGD'+addCommas(Math.round((mindeathcover)))+' - SGD'+addCommas(Math.round(maxdeathcover)));
			   console.log(minci+'|'+ci);
			   var mincicover = (minci + ci) *  jQuery('#income').val();
			   var maxcicover = (maxci + ci) *  jQuery('#income').val();
			   console.log(minci+'|'+ci+'|'+mincicover);
			   var l2 = cl *  jQuery('#income').val();

			    jQuery('.mincicover').text('SGD'+addCommas(Math.round(l2)));
			    jQuery('.maxcicover').text('SGD'+addCommas(Math.round(mincicover))+' - SGD'+addCommas(Math.round(maxcicover)));
			   jQuery('.coverage_result_container').show();
			   
			   var titledsc = "Planning a family Coverage Form Submit;age="+jQuery('#age').val()+";income="+jQuery('#income').val();
			   
			   jQuery.ajax({
				type: "POST",	
				url:jQuery('#base_url').val()+"/sugar.php",		       
				data:'url='+window.location.href+'&desc='+titledsc,					  
				success: function(data){
					
			  }
			});
			
          }
		  function addCommas(nStr)
		  {
			   nStr += '';
			   x = nStr.split('.');
			   x1 = x[0];
			   x2 = x.length > 1 ? '.' + x[1] : '';
			   var rgx = /(\d+)(\d{3})/;
			   while (rgx.test(x1)) {
			   x1 = x1.replace(rgx, '$1' + ',' + '$2');
			   }
			   return x1 + x2;
		  }
		</script>
      	<div class="coverage_result_container">
     <h3>Based on your age and income</h3>
     <div class="tab_section">
     	<ul>
     		<li class="tabs active" rel="life-cover"><a href="javascript:void(0)">Life <span>Cover</span></a></li>
     		<li class="tabs" rel="critical-illness-cover"><a href="javascript:void(0)">Critical <span>Illness Cover</span></a></li>
     	</ul>
     </div>
     <div class="tab_content_container">
     	<div class="tab_content" id="life-cover">
     	<div class="coverage_box_container">
     		<div class="coverage_box">
     		<i class="icon icon1"></i>
     		<p>Others like you have<br/> an average cover of*</p>
     		<div class="price mindeathcover">$200,000</div>
     		</div>
     		<div class="coverage_box">
     		<i class="icon icon2"></i>
     		<p>However, people like you <br/><span>need</span> an average cover of</p>
     		<div class="price maxdeathcover">$350,000</div>
     		</div>
     		</div>
     		<p class="text-center">*Based on a survey conducted by Singapore Life.</p>
     		<p class="text-center" style="color:#13abd3">Assuming that people like you will spend half to three quarter of your income to provide for your future family and kids and that this will reduce once your kids reach age 20.</p>
     	</div>
     	<div class="tab_content" id="critical-illness-cover">
		  <div class="coverage_box_container">
     		<div class="coverage_box">
     		<i class="icon icon1"></i>
     		<p>Others like you have<br/> an average cover of*</p>
     		<div class="price mincicover">$100,000</div>
     		</div>
     		<div class="coverage_box">
     		<i class="icon icon2"></i>
     		<p>However, people like you <br/><span>need</span> an average cover of</p>
     		<div class="price maxcicover">$200,000</div>
     		</div>
     		</div>
     		<p class="text-center">*Based on a survey conducted by Singapore Life.</p>
     		<p class="text-center" style="color:#13abd3">Assuming that people like you will spend half to three quarter of your income to provide for your future family and kids and that this will reduce once your kids reach age 20.</p>
		  
		  
		</div>
     </div>
      <div class="btn_block">
      	<a href="<?php echo $code ?>" target="_blank" class="btn primary-btn">GET A QUOTE</a>
      </div>
     
      	</div>
		</div>
      </div>