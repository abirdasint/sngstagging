<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>

<?php
global $base_url;   // Will point to http://www.example.com
global $base_path;  // Will point to at least "/" or the subdirectory where the drupal in installed.
$nid = '';
if ($node = menu_get_object()) {
    $nid = $node->nid;
}
$path=drupal_get_path_alias('node/'.$nid);
$code = variable_get('getaquote');

?>
<div id="page-wrapper">

<div id="page">

  <!-- Header -->
	<div id="header" class="nonsticky">
	
			<div class="section clearfix">
				<!--<a href="#" id="logo"><img src="images/logo.png"/></a>-->
				<?php if ($logo): ?>
				<div class="logo_section">
				  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				  </a>
				  </div>
				<?php endif; ?>
				<div class="right_block clearfix">	
				<div class="nav_block">			
				<div class="toggle_menu_btn hide_desktop">
					<span></span>
				</div>
				<?php print render($page['header_menu']); ?>
				</div>
				<div class="nav_right_block">				
				<div class="account_link hide_mobile">
				<a href="javascript:void(0);">My Account</a>
				<div class="account_dropdown">
					<ul>
						<li><a href="https://portal.singlife.com">Login</a></li>
						<li><a href="http:///admin.singlife.com">Login as Adviser</a></li>
						<!--<li><a href="https://portal.singlife.com/en-gb/Process/InsuranceQuotation/DocumentUploadStart">Submit Documents</a></li> -->
					</ul>
				</div>
				</div>
				<div class="phone_icon">
        <?php
        $block = module_invoke('block', 'block_view', '28');
        print render($block['content']);
        ?>
        </div>
				<div class="quote_link hide_mobile"><a href="<?php echo $code ?>" target="_blank" >Get A Quote</a></div>
				</div>
				</div>
			</div>
	
		</div>
<!--MOBILE MENU-->
<?php print render($page['highlighted']); ?>
<!--MOBILE MENU END-->
<!-- /Header -->

<!--EARLYBIRDPROMO MESSAGE POPUP STRAT-->
 <?php
  if ($messages!='' && $path=='earlybirdpromo'): ?>
    <div id="scrollm1" class="mobile_menu_backdrop" style="display:block"></div>
    <div id="messages1" class="after-contactus-submit" style="display:block">  
    <h2><div class="close">close</div></h2>  
      <?php print $messages; ?>    
      <div class="btn_block">
      	<a href="javascript:void(0);" onclick="window.location.href = '<?php echo base_path(); ?>';jQuery('#scrollm1').hide();jQuery('#messages1').hide();">VISIT OUR HOMEPAGE</a>
      	<a href="javascript:void(0);" onclick="jQuery('#scrollm1').hide();jQuery('#messages1').hide();scrolldiv();">Retry</a>
      </div>
    </div> 
  <?php endif; ?>
<!--EARLYBIRDPROMO MESSAGE POPUP END-->


<!--TAXIPROMO MESSAGE POPUP STRAT-->
 <?php
  if ($messages!='' && $path=='taxipromo' || $messages!='' && $path=='promotion'): ?>
    <div id="scrollm1" class="mobile_menu_backdrop" style="display:block"></div>
    <div id="messages1" class="taxipromopoup" style="display:block">  
    <h2><span class="close hompopclose"><span></span></span></h2>  
    <div class="msg">
      <?php print $messages; ?>    
      </div>
    </div> 
  <?php endif; ?>
<!--TAXIPROMO MESSAGE POPUP END-->



<?php if ($messages!='' && $nid!=95 && $path!='earlybirdpromo'): ?>
<div id="messages"><div class="section clearfix">
	<?php print $messages; ?>
</div></div> <!-- /.section, /#messages -->

<?php endif; ?>
	 <script>
	function scrolldiv() {
jQuery('html, body').animate({
		scrollTop: jQuery("#form-example-discount-form, #form-example-earlybird-form").offset().top-100
}, 1000);      }
	jQuery( window ).load(function() {
        console.log('test');
        jQuery(".expanded").each(function() {
                console.log(jQuery(this).find('a:first'));
                jQuery(this).find('a:first').attr("href", "#");
    
        });
    });
	</script>
	 <?php
if ($messages!='' && $nid==95): ?>
	
	 <div id="scrollm" class="mobile_menu_backdrop"></div>
<div id="messages" class="after-contactus-submit">  
<h2><div class="close">close</div></h2>  
	<?php print $messages; ?>    
	<div class="btn_block">
		<a href="javascript:void(0);" onclick="window.location.href = '<?php echo base_path(); ?>';jQuery('#scrollm').hide();jQuery('#messages').hide();">SIGN-UP ON HOME PAGE</a>
		<a href="javascript:void(0);" onclick="jQuery('#scrollm').hide();jQuery('#messages').hide();scrolldiv();">Retry</a>
	</div>
</div> <!-- /.section, /#messages -->
<?php endif; ?>


<!--for claims page success popup start-->
<?php
if ($messages!='' && $path=='claims' && (isset($_SESSION['claim_success']) && ($_SESSION['claim_success']==1))): ?>
<style type="text/css">
#messages{display:none;}
</style>	
<div class="mobile_menu_backdrop" style="display:block"></div>
<div class="after-contactus-submit claims_popup" style="display:block">  
<h2><div class="close"><span>close</span></div></h2>  
<div class="msg">
<?php 
$block =block_load('block',20);
   $output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));        
   print $output;
   unset($_SESSION['claim_success']);
?> 
</div>	
</div>
<?php endif; ?>
<!--for claims page success popup start-->
<div id="main-wrapper" class="clearfix">
   <?php if($path=='promotion'): ?>
	<div id="taxipromo">.</div>
	<?php endif; ?>
  <div id="main" class="clearfix">

    <?php //if ($breadcrumb): ?>
      <!--<div id="breadcrumb"><?php print $breadcrumb; ?></div>-->
    <?php //endif; ?>
   
	 <?php if ($page['featured_video']): ?>
      <?php print render($page['featured_video']); ?>
    <?php endif; ?>
	
	<?php if ($page['featured_video_underneath']): ?>
      <?php print render($page['featured_video_underneath']); ?>
    <?php endif; ?>
	
	<?php if ($page['calculation_form']): ?>
      <?php print render($page['calculation_form']); ?>
    <?php endif; ?>

   

    <div id="content" class="column">
	<div class="section">
	<?php print $messages; ?>
       <?php //if ($title): ?>
        <!--<h1 class="title" id="page-title">
          <?php //print $title; ?>
        </h1>-->
      <?php //endif; ?>
      <?php print render($page['content']); ?>
      <?php if($path == 'universal-life'){ ?>
      <div class="mobile_menu_backdrop" style="display: none;"></div>
      <div class="getconnected after-contactus-submit" style="display: none;">
        <h2><span class="close">close</span></h2>
        <div class="msg">
            <div class="newsleter_form myform">
             <div class="field_block">
   <label>Name <em>*</em></label>
  <input type="text" id="name" class="form-text">
   <p style="display:none;"  id="lifename" for="edit-assured" generated="true" class="error">Name is required.</p>
  </div>
              <div class="field_block">
   <label>Email <em>*</em></label>
  <input type="text" id="email" class="form-text">
   <p style="display:none;"  id="lifeemail" for="edit-assured" generated="true" class="error">Email is not valid.</p>
  </div>
               <div class="field_block">
   <label>Nationality <em>*</em></label>
  <input type="text" id="nationality" class="form-text">
   <p style="display:none;"  id="lifenationality" for="edit-assured" generated="true" class="error">Nationality is required.</p>
  </div>
                <div class="field_block">
   <label>Residence <em>*</em></label>
  <input type="text" id="country_of_residence" class="form-text">
   <p style="display:none;"  id="lifecountry_of_residence" for="edit-assured" generated="true" class="error">Residence is required.</p>
  </div>
  <div class="field_block radio_block">
   <div class="label">Do you have a current private bank relationship? <em>*</em></div>
   
    <div class="field_block">
   <input type="radio" class="relationshipr" name="relationship" value="1" >Yes
   </div>
    <div class="field_block">
   <input type="radio" class="relationshipr" name="relationship" value="0" checked="checked">No   
  </div>
   <div class="field_block bankname" style="display: none;">   
   <input type="text" id="currentbank" class="form-text" placeholder="Current bank *"/>
    <p style="display:none;"  id="lifecurrentbank" for="edit-assured" generated="true" class="error">Bank is required.</p>
  </div>
</div>
  <div class="btn_block">
  <input type="button" id="lifebutton" value="Submit" onclick="lifepop();" class="btn primary-btn">
  </div>
            
         <?php //print render($page['getconnected']); ?>
         </div>
         <div class="success_msg" style="display: none;">
      <h3>Thank You!</h3>   
      <p>We have received your request and will process it shortly. </p>
      </div>
      </div>
     
      
	  <?php if ($page['featured_news']): ?>
	  <div class="home_block5 animatedParent">
		<div class="conatiner">
		
			<?php print render($page['featured_news']); ?>
		</div>
	 </div>
     <?php endif; ?>
     
	 <?php if ($page['press']): ?>
		<?php print render($page['press']); ?>
     <?php endif; ?>
	 
    </div>
<?php } ?>
</div> <!-- /.section, /#content -->

 

  </div>
</div><!-- /#main, /#main-wrapper -->



  <div id="footer-wrapper">
			<div class="section">
				<div id="footer-columns" class="clearfix">
					<div class="footer_block1">
						<p>Singapore Life is licensed by the Monetary Authority of Singapore and your policies are protected by the <br/><a href="<?php echo base_path(); ?>policyownersprotectionscheme">Policy Owner's Protection Scheme.</a></p>
					</div>                
					<div class="footer_block2">	
					<div class="btn_block">									
					<a href="<?php echo $code ?>" target="_blank" class="btn primary-btn">Get a Quote</a>
					</div>
					<?php
          $block = module_invoke('block', 'block_view', '27');
          print render($block['content']);
          ?>
					<div class="chat_block"><i class="icon"></i> <a href="javascript:$zopim.livechat.window.show();">Ask us</a></div>							
					</div>
                    
					<div class="footer_block3 clearfix">                   
					<div class="column">
					<?php print render($page['footer_firstcolumn']); ?>
					</div>
					<div class="column">
					<?php print render($page['footer_secondcolumn']); ?>
					</div>
					<div class="column">
					<?php print render($page['footer_thirdcolumn']); ?>
					</div><!---->
                  
                   
                   
                    
					<div class="column">
						<div class="social_media_block">
							<?php print render($page['footer_fourthcolumn']); ?>
						</div>
					</div><!---->
					
					</div>
					
					<div class="footer_block4">
                    
						<ul>
							<li><a href="<?php echo base_path(); ?>terms-of-use">Terms of Use</a></li>							
							<li><a href="<?php echo base_path(); ?>privacy">Privacy Policy</a></li>
							<li><a href="<?php echo base_path(); ?>fair-dealing">Fair Dealing</a></li>	
							<li><a href="<?php echo base_path(); ?>financial-advice">Financial Advice</a></li>						
						</ul>
                      
						<p class="copyright">&copy; Singapore Life 2017</p>
					</div>
				</div>
			</div>
		</div>
		<div class="backToTop">Back to top</div>
	</div>
</div>	
<?php if($path == 'contact-us'){ ?>
<div class="mobile_menu_backdrop"></div>
<div class="after-contactus-submit">

<h2><span class="close">close</span></h2>
<div class="msg">
<h3>Thank you.</h3>
Your message has been received and we will be contacting you shortly to follow up.
</div>

</div>
<?php }?>
<?php 
$path =  request_path();
if($path== 'contact-us'){
?>
    <script>
        
      var map;
      var uluru = {lat: 1.306004, lng: 103.791510};

      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: new google.maps.LatLng(1.306004, 103.791510),
          mapTypeId: 'roadmap',
           zoomControl: false,
           // zoom: 7,
            scrollwheel: false,
            
        });
        

       


        //var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icons = {
          parking: {
            icon: themeurl + '/images/map_marker.png'
            },        
        };    
          var marker = new google.maps.Marker({
            position: uluru,
            icon: icons['parking'].icon,
            map: map
          
            
          });
         var contentString = '<div style="text-align:left"><h4>Singapore Life</h4><p style="font-size:14px; margin-top:10px;">11 North Buona Vista Drive,<br/> #13-07 The Metropolis Tower 2,<br/> Singapore 138589</p></div>';
var infowindow = new google.maps.InfoWindow({
    content: contentString,
    maxWidth: 200
  });
marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrt_sspad67XALpVwT6EZQJ9gjOULLyR4&callback=initMap">
    </script>
  <?php
    }
?>
<script>
    function lifepop(){
		//alert('hiii');
       //  jQuery('#btpop').val('Please wait..');
	//jQuery('#newsleter_form').removeClass('errorhome');
		//jQuery('#emailvalhome').hide();
        var error = 1;
		var emailAddress = jQuery('#homepopemail').val();
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if(jQuery('#name').val()==''){
            error = 2;
            jQuery('#lifename').show();
        }else{
            jQuery('#lifename').hide();
        }
        if(jQuery('#email').val()=='' || !pattern.test(jQuery('#email').val())){
            error = 2;
            jQuery('#lifeemail').show();
        }else{
            jQuery('#lifeemail').hide();
        }
        if(jQuery('#nationality').val()==''){
            error = 2;
            jQuery('#lifenationality').show();
        }else{
            jQuery('#lifenationality').hide();
        }
        if(jQuery('#country_of_residence').val()==''){
            error = 2;
            jQuery('#lifecountry_of_residence').show();
        }else{
            jQuery('#lifecountry_of_residence').hide();
        }
      // alert(jQuery("input[name='relationship']:checked").val());
      
      if(jQuery("input[name='relationship']:checked").val() == 1){
        if(jQuery('#currentbank').val()==''){
            error = 2;
            jQuery('#lifecurrentbank').show();
        }else{
            jQuery('#lifecurrentbank').hide();
        }      
      }
      if(error == 2){
        return false;
      }
        
		//if(pattern.test(emailAddress)){
			//console.log(jQuery('#base_url').val());
//           jQuery('.newsletter_popup').fadeOut(function(){
//		jQuery('.mobile_menu_backdrop').fadeOut();
//	});
	//jQuery('.getconnected').fadeOut();
	//jQuery('.mobile_menu_backdrop').fadeOut();
	jQuery('.newsleter_form').slideUp();
	jQuery('.success_msg').slideDown();
    jQuery.ajax({
        type: "POST",	
        url:jQuery('#base_url').val()+"/universal.php",		       
        data:"name="+jQuery('#name').val()+"&email="+jQuery('#email').val()+"&nationality="+jQuery('#nationality').val()+"&country_of_residence="+jQuery('#country_of_residence').val()+"&relatiobship="+jQuery("input[name='relationship']:checked").val()+"&bank="+jQuery('#currentbank').val(),				  
        success: function(data){
            //console.log(data);
            //jQuery('#btpop').val('keep me informed');
        
        }
    });
			
//			
//		}else{
//             jQuery('#btpop').val('keep me informed');
//			jQuery('#emailvalhome').show();
//			jQuery('#homepopemail').focus();
//			jQuery('.newsleter_form').addClass('errorhome');
//
//		}
		
	}
    jQuery( document ).ready(function() {
        jQuery('.relationshipr').click(function(){
            if(jQuery("input[name='relationship']:checked").val() == 1){
                jQuery('.bankname').show();    
            }else{
                jQuery('.bankname').hide();
            }
        })    
    });
</script>
<!--COLLECT -->
<?php 
if($_COOKIE['homeemail']!=1){
	

?>
<script>
	function homepop(){
		//alert('hiii');
         jQuery('#btpop').val('Please wait..');
		jQuery('#newsleter_form').removeClass('errorhome');
			jQuery('#emailvalhome').hide();
		var emailAddress = jQuery('#homepopemail').val();
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		if(pattern.test(emailAddress)){
			//console.log(jQuery('#base_url').val());
           jQuery('.news_form').slideUp(function(){
		jQuery('.success_msg').slideDown();
	});
			
			jQuery.ajax({
			type: "POST",	
			url:jQuery('#base_url').val()+"/home.php",		       
			data:"email="+emailAddress,					  
			success: function(data){
			//console.log(data);
             jQuery('#btpop').val('keep me informed');
			
			}
		});
		
		jQuery.ajax({
			type: "POST",	
			url:jQuery('#base_url').val()+"/sugar.php",		       
			data:'url='+window.location.href+'&email='+emailAddress+'&desc=homepopemail',					  
			success: function(data){
				
		  }
		});
			
			
		}else{
             jQuery('#btpop').val('keep me informed');
			jQuery('#emailvalhome').show();
			jQuery('#homepopemail').focus();
			jQuery('.newsleter_form').addClass('errorhome');

		}
		
	}
	
</script>
   <div class="newsletter_popup_other">
   <h2><span class="close hompopclose"><span></span></span></h2>
   <div class="msg">
   <div class="news_form">
   <h2>Thanks for dropping by!</h2>
   <p>
   	But life is too short to just hang around.
   </p>
   <p>Share your email with us to learn about our latest promotions.</p>
   <div class="newsleter_form myform">
   <div class="field_block">
   <label>Your Email</label>
  <input type="text" id="homepopemail" class="form-text">
   <p style="display:none;"  id="emailvalhome" for="edit-assured" generated="true" class="error">Email is not valid.</p>
  </div>
  <div class="btn_block"> 
 <input type="button" id="btpop" value="keep me informed" onclick="homepop();" class="btn primary-btn">
 </div>
 </div>
 </div>
   <div class="success_msg" style="display: none;">
      <h2>Thank You!</h2>   
      <p>We have received your email. </p>
      </div>
  </div>
</div><div class="newsletter_overaly newsletter_overaly_other"></div>
<?php } ?>
<!--COLLECT END-->